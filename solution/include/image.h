#pragma once

#include <stdint.h>

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel *getPixel(uint64_t x, uint64_t y, const struct image *image);

struct image createImage(uint64_t width, uint64_t height);

void destroyImage(struct image *image);
