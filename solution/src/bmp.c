#include "../include/bmp.h"

#define PADDING 4
#define TYPE 0x4D42
#define INFO_HEADER_SIZE 40
#define RESERVED 0
#define PLANES_NUMBER 1
#define BITS_COUNT 24
#define NO_COMPRESSION 0
#define PPX 0
#define COLORS 0

uint8_t paddingCalculate(uint64_t width) {
    return PADDING - (width * sizeof(struct pixel)) % PADDING;
}

struct __attribute__((packed)) bmpHeader {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bmpHeader writeHeader(uint32_t width, uint32_t height) {
    const uint32_t headerSize = sizeof(struct bmpHeader);
    const uint32_t imageSize = (sizeof(struct pixel) * width + paddingCalculate(width)) * height;
    return (struct bmpHeader) {
            TYPE,
            headerSize + imageSize,
            RESERVED,
            headerSize,
            INFO_HEADER_SIZE,
            width,
            height,
            PLANES_NUMBER,
            BITS_COUNT,
            NO_COMPRESSION,
            imageSize,
            PPX,
            PPX,
            COLORS,
            COLORS
    };
}

enum readStatus fromBmp(FILE *file, struct image *image) {
    if (file != NULL && image != NULL) {
        struct bmpHeader header;

        if (fread(&header, sizeof(struct bmpHeader), 1, file) == 1) {
            *image = createImage(header.biWidth, header.biHeight);

            if (image->width != 0) {
                for (uint64_t countHeight = 0; countHeight < image->height; countHeight++) {
                    if (fread(getPixel(0, countHeight, image), sizeof(struct pixel), image->width, file) != image->width
                        || fseek(file, paddingCalculate(image->width), SEEK_CUR) != 0) {
                        destroyImage(image);
                        return READ_ERROR;
                    }
                }
                return READ_OK;
            } else
                destroyImage(image);
        }
    }
    return READ_ERROR;
}

enum writeStatus toBmp(FILE *file, struct image const *image) {
    if (file != NULL && image != NULL) {
        const struct bmpHeader header = writeHeader(image->width, image->height);

        if (fwrite(&header, sizeof(struct bmpHeader), 1, file) == 1) {
            for (uint64_t countHeight = 0; countHeight < image->height; countHeight++) {
                uint8_t padding = paddingCalculate(image->width);
                char plug[PADDING] = "";

                if (fwrite(getPixel(0, countHeight, image), sizeof(struct pixel), image->width, file) != image->width
                    || fwrite(&plug, sizeof(uint8_t), padding, file) != padding)
                    return WRITE_ERROR;
            }
            return WRITE_OK;
        }
    }
    return WRITE_ERROR;
}
