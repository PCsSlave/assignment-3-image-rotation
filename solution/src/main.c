#include "../include/bmp.h"
#include "../include/transformation.h"
#include <stdio.h>

int main(int argc, char **argv) {
    if (argc == 3) {
        FILE *fileImageIn = fopen(argv[1], "rb");

        if (fileImageIn != NULL) {
            struct image data = {0};
            if (fromBmp(fileImageIn, &data) != READ_ERROR) {
                if (fclose(fileImageIn) == EOF)
                    printf("CLOSE_ERROR");
                struct image resultImage = rotateImage(data);
                destroyImage(&data);
                FILE *fileImageOut = fopen(argv[2], "wb");

                if (fileImageOut != NULL) {
                    if (toBmp(fileImageOut, &resultImage) != WRITE_ERROR) {
                        destroyImage(&resultImage);
                    }
                    if (fclose(fileImageOut) == EOF)
                        printf("CLOSE_ERROR");
                }
            }
        }
    } else {
        printf("ARGUMENTS_ERROR");
        return -1;
    }
    return 0;
}
